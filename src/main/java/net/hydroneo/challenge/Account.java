package net.hydroneo.challenge;

public class Account {

    public String id;
    public String name;
    public int age;
    public Address address;

    public Account(String id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public Account(String id, String name, int age, Address address) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.address = address;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", address=" + address +
                '}';
    }
}
