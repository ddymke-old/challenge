package net.hydroneo.challenge;

import java.util.Collection;
import java.util.function.Predicate;

public interface KeyValueStore<VALUE> {

    void put(String key, VALUE value);

    VALUE get(String key);

    Collection<VALUE> list(Predicate<VALUE> predicate);
}
