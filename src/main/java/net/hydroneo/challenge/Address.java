package net.hydroneo.challenge;

public class Address {

    public String zip;
    public Type type;

    public Address(String zip, Type type) {
        this.zip = zip;
        this.type = type;
    }

    public String getZip() {
        return zip;
    }

    public Type getType() {
        return type;
    }

    public enum Type {
        VILLAGE,
        CITY
    }
}
