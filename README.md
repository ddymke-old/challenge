## Previous Projects

- Responsibilities
- Architecture
- pros / cons

## Questions

### General
1.) What are Lambda Functions, Functions, Methods and how do they differentiate?

2.) What are Class and Object?

3.) Favourite Software Design Pattern - how does it work and why do you like it?

4.) What is an interface?

5.) What does Dependency Injection mean to you?

6.) What do you understand under Clean Code?

7.) Ever heard about Kanban? What is it? Do you like it

8.) Do you know the SOLID principles?

9.) Did you work with MQTT, Kafka, MongoDB, Reactor / Webflux before?

### Java
1.) What does Boxing / Unboxing mean?

2.) What are the major features introduced by Java 8 and Java 11?

3.) When would you use a LinkedList when an ArrayList?

4.) What is the difference between map and flatmap

  
### Spring

1.) What is Spring?
 
2.) When to use @Component, @Service, @Repository, @RestController, @Bean

## Challenge

Part 1.)

Please implement the interfaces KeyValueBucketStore and KeyValueBucket. A KeyValueBucket is an entity which maps a 
value to a given key - just think of key value store. A KeyValueBucketStore maps KeyValueBuckets to a key.

The implementation MUST be THREAD-SAFE and IMMUTABLE.

Example use case:
```java
var store = new KeyValueStoreImpl<Account>();
store.put("a", new Account("a","Adam",18));
store.put("b", new Account("b", "Berta", 24));
store.put("c", new Account("c", "Charlie", 23));

store.put("activeUserBucket", activeUserBucket);

var adam = store.get("a");
adam.age = 30;

store.findAll(account -> account.age < 23)  
     .stream()
     .forEach(account -> System.out.println(account));

```

Part 2.)

Make the implementation of Account immutable. 
Hint: Don't forget about Reflection. 